angular.module('starter.controllers', ['ion-cool-profile'])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  
})

.controller('BrowseController', function($scope,$state,$cordovaCamera) {

    
    navigator.geolocation.getCurrentPosition(showPosition);
    function showPosition(position) {
      alert(position.coords.latitude + "," + position.coords.longitude);
  }


  
  
 $scope.TomarFoto = function()
 {
  navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
    destinationType: Camera.DestinationType.DATA_URL
 }); 
 }

function onSuccess(imageData) {
    
  console.log(imageData);

}

function onFail(message) {
    alert('Failed because: ' + message);
}

 
  

  
})

.controller('PlaylistsCtrl', function($scope,$state,$http) {
  $scope.lstFotos = [];

  $scope.ObtenerFotos = function()
  {
    $http({
      headers: {
         'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      },
      method: "POST",
      url: 'http://tkcore.ddns.net:91/Imagenes.asmx/ObtenerImagenes',
      data: $.param({
        cImei: "ruben"
      }),
    }).success(function(response) {
        debugger
        $scope.lstFotos = response.lstImagenes;
       
      }).error(function(data, status, headers, config) {
        debugger
        alert('The username and/or password is wrong.');
      });
  }


  

  
})

.controller('PlaylistCtrl', function($scope, $stateParams,$http) {

  $scope.nIdImagen = $stateParams.nIdImagen;

  $http({
    headers: {
       'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    },
    method: "POST",
    url: 'http://tkcore.ddns.net:91/Imagenes.asmx/ObtenerImagen',
    data: $.param({
      nIdImagen: $scope.nIdImagen
    }),
  }).success(function(response) {
      debugger
      $scope.cFoto = response.Imagen.cFoto;
      
      var map;
      map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: response.Imagen.nLatitud, lng: response.Imagen.nLongitud},
        zoom: 18,
        scrollwheel: true,
      });
      var marker = new google.maps.Marker({
        position: {lat: response.Imagen.nLatitud, lng: response.Imagen.nLongitud},
        map: map
    });

     
    }).error(function(data, status, headers, config) {
      debugger
      alert('The username and/or password is wrong.');
    });


  
});
